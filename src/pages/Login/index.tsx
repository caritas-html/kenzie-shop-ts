import { TextField, Button } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";
import * as yup from "yup";
import { useAuth } from "../../providers/Auth";
import { useHistory } from "react-router";
import { Container } from "./styles";

interface IFormInput {
  username: string;
  password: string;
}

const Login = () => {
  const { signIn, auth } = useAuth();
  const [error] = useState<boolean>(false);
  const history = useHistory()

  const handleRedirect = () => {
    history.push("/register")
  }

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    password: yup
      .string()
      .min(6, "Mínimo de 6 dígitos")
      .required("Campo obrigatório"),
  });

  const { register, handleSubmit, formState: { errors } } = useForm<IFormInput>({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: object) => {
    signIn(data);
    history.push("/dashboard")
  };

  if (auth) {
    history.push("/dashboard")
  }

  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div>
          <TextField
            margin="normal"
            variant="outlined"
            label="Nome de usuário"
            size="small"
            color="primary"
            {...register('username')}
            error={!!errors.username}
            helperText={errors.username?.message}
          ></TextField>
        </div>

        <div>
          <TextField
            type="password"
            margin="normal"
            variant="outlined"
            label="Senha"
            size="small"
            color="primary"
            {...register('password')}
            error={!!errors.password}
            helperText={errors.password?.message}
          ></TextField>
        </div>
        <div style={{display: "flex", justifyContent: "space-between"}}>
          <Button type="submit" variant="contained" color="primary" size="large">
            Enviar
          </Button>
          <Button variant="contained" color="primary" size="medium" onClick={() => handleRedirect()}>
            Registrar
          </Button>
        </div>
      </form>
      {error && <span> Usuário ou senha incorretas! </span>}
    </Container>
  );
}

export default Login;