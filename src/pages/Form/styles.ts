import styled, { keyframes } from "styled-components";
import {ReactComponent as Dashboardsvg} from '../../assets/order.svg';

const appearFromRight = keyframes`
    from{
        opacity:0;
        transform: translateX(150px)
    }
    to{
        opacity: 1;
        transform: translateX(0px)
    }

`
export const SvgOne = styled(Dashboardsvg)`
  position: absolute;
  right: 0;
  bottom: 0;
  width: 400px;
  height: 400px;
  z-index: -1;
  animation: ${appearFromRight} 1s;
`;

export const Container = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 15px;
  flex-direction: column;
  width: 100%;
  height: 100vh;
  margin: 0 auto;
  
`;

export const Image = styled.img`
  width: 120px;
  height: 220px;
  margin-left: 15px;
`;

export const CardContainer = styled(Container)`
  padding: 0 0 8px;
  justify-content: space-between;
  margin-top: 0 auto;
  
  
    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      width: 70%;
      margin: 0 auto;
      label {
        display: none;
      }
    } 
    div {
      width: 90%;
      margin-bottom: 10px;
      display: flex;
      justify-content: space-between;
    }
`;

export const Container404 = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  button {
    margin-top: 15px;
  }
`;


