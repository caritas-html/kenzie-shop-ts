import api from "../../services/api";
import { Button, TextField } from "@material-ui/core"
import { useForm } from "react-hook-form";
import { Container, CardContainer, SvgOne } from './styles'
import { useHistory } from "react-router-dom";
import { toast } from 'react-toastify'

interface SignIn {
  username: string;
  password: string;
  email: string;
}

const Form = () => {
  const history = useHistory();

  const { register, handleSubmit } = useForm<SignIn>();

  const submitFunc = (userData: SignIn) => {
    api.post("https://kenzieshop.herokuapp.com/users/", userData)
    .then(res => 
      toast.success("a", {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      }))
    history.push("/login")

  }

  return (
    <Container>
      <CardContainer>
        <form onSubmit={handleSubmit(submitFunc)}>
        <label>Username</label>
        <div><TextField fullWidth={true} variant="outlined" placeholder="username" {...register("username")} /></div>
        <label>Password</label>
        <div><TextField type="password" fullWidth={true} variant="outlined" placeholder="password"{...register("password")} /></div>
        <label>Email</label>
        <div><TextField fullWidth={true} variant="outlined" placeholder="email"{...register("email")} /></div>
          <div><Button type="submit" variant="contained" color="primary" size="medium">Cadastrar</Button></div>
      </form>
    </CardContainer>
    <SvgOne/>
    </Container>
  )
};

export default Form;