import { useAuth } from "../../providers/Auth";
import { Redirect } from "react-router";

const Dashboard = () => {
  const { auth} = useAuth();


  if(!auth){
    return <Redirect to="/login"/>
  }

  return (
    <h1>

      Dashboard
      <button
        onClick={() => {
          localStorage.clear();
          window.location.reload();
        }}
      >
        Logout
      </button>
    </h1>
  );
};

export default Dashboard;