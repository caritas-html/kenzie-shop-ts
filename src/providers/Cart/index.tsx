import { createContext, ReactNode, useContext, useEffect, useState } from "react";


interface CartProviderData {
  cart: Products[];
  setCartFunc: (param: Products[]) => void
}

interface CartProviderProps {
  children: ReactNode
  
}

export interface Products {
  description: string;
  id: number;
  image: string;
  price: number;
  priceFormatted: string;
  title: string
}
const CartContext = createContext<CartProviderData>({} as CartProviderData);

export const CartProvider = ({ children }: CartProviderProps) => {
  
  var storageCart = localStorage.getItem("cart")
  var getItem = storageCart ? JSON.parse(storageCart) : []

  const [cart, setCart] = useState<Products[]>(getItem);

  const setCartFunc = (param: Products[]) => {
    setCart(param)
  }

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  return (
    <CartContext.Provider value={{ cart, setCartFunc }}>
      {children}
    </CartContext.Provider>
  );
};
export const useCart = () => useContext(CartContext);
